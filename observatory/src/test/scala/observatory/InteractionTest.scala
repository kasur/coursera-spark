package observatory

import java.io.File
import java.time.LocalDate

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class InteractionTest extends FlatSpec {

  behavior of "Map Interaction"

  it should "generate tiles properly for a single year" in {
    type Data = Iterable[(Location, Double)]

    for {
      year <- (2000 to 2015).par
    } {
      val yearData = TestHelpers.readYear(year)

      Interaction.generateTiles[Data](Iterable(year -> yearData), (year: Int, zoom: Int, x: Int, y: Int, data: Data) => {

        val image = Interaction.tile(data, ColorSchema.colorPoints, zoom, x, y)

        val root: File = new File(s"target/temperatures/$year/$zoom")

        root.mkdirs()

        image.output(new File(root, s"$x-$y.png"))

        println(s"Generated tile for (year=$year, zoom=$zoom, x=$x, y=$y)")

        ()

      })
    }

  }
}
