package observatory

import java.io.File

import com.sksamuel.scrimage.Image
import observatory.TestHelpers.readYear
import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class Visualization2Test extends FlatSpec {

  behavior of "Visualization Logic"

  it should "generate tiles properly for a single year" in {

    type YearlyData = Iterable[(Location, Double)]
    type NormalYearsData = Iterable[YearlyData]
    type Grid = (Int, Int) => Double
    type DeviationGrid = (Int, Int) => Double

    val normalYears: Seq[Int] = 1975 to 1989
    val deviationYears: Seq[Int] = 1990 to 2015
    val zooms: Seq[Int] = 0 to 3

    println("[erusak] Step 1")
    val normalYearsData: NormalYearsData = normalYears map readYear

    println("[erusak] Step 2")
    val normalGrid: Grid = Manipulation.average(normalYearsData)

    println("[erusak] Step 3")
    val deviationGrid: YearlyData => DeviationGrid = (yearlyData: YearlyData) =>
      Manipulation.deviation(yearlyData, normalGrid)

    println("[erusak] Step 4")
    for {
      year <- deviationYears
      yearlyData = readYear(year)
      //println("[erusak] Step 5")
      yearDeviation: DeviationGrid = deviationGrid(yearlyData)
      zoom <- zooms
      pow = math.pow(2, zoom).toInt
      x <- 0 until pow
      y <- 0 until pow
    } {

      println(s"[erusak] Step 6 (zoom=$zoom, x=$x, y=$y)")
      val image: Image = Visualization2.visualizeGrid(yearDeviation, ColorSchema.colorPointsForDeviation, zoom, x, y)

      val root: File = new File(s"target/deviations/$year/$zoom")

      root.mkdirs()

      image.output(new File(root, s"$x-$y.png"))

      ()

    }
  }

}
