package observatory

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

/**
  * Created by kasured on 5/6/17.
  */
@RunWith(classOf[JUnitRunner])
class GeneralTest extends FlatSpec {
  import org.apache.log4j.{Level, Logger}
  Logger.getLogger("org.apache.spark").setLevel(Level.WARN)

  behavior of "General Test"
  it should "Successfully initialize local spark context" in {
    println(init.sparkAware.spark.conf.getAll)
  }

  it should "find resource files" in {

  }
}
