package observatory


import java.io.File
import java.time.LocalDate

import observatory.ColorSchema.{PointFor, colorPoints, rgbFor, tempFor}
import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class VisualizationTest extends FlatSpec {
  behavior of "Visualization Logic"

  it should "correctly interpolate colors for temperature" in {

    Seq(PointFor('white), PointFor('black)).foreach({
      case point @ (temp, color) =>
        assert(Visualization.interpolateColor(Iterable(point, point), temp) === color)
    })

  }

  it should "select a given point if it already exists" in {

    colorPoints foreach {
      case (temp, color) =>
        assert(Visualization.interpolateColor(colorPoints, temp) === color)
    }

  }

  it should "give correct linear color estimation" in {
    assert(Visualization.interpolateColor(colorPoints, tempFor('orange)) === rgbFor('orange))

  }

  it should "produce image for test values" in {

    val year: Int = 2015

    val observations: Iterable[(LocalDate, Location, Double)] =
      Extraction.locateTemperatures(year, "/stations.csv",s"/$year.csv")

    val avgs: Iterable[(Location, Double)] = Extraction.locationYearlyAverageRecords(observations)

    val image = Visualization.visualize(avgs, colorPoints)

    image.output(new File(s"target/$year.png"))

    ()
  }

}
