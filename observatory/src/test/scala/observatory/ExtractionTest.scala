package observatory

import java.time.LocalDate

import org.junit.runner.RunWith
import org.scalatest.FlatSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ExtractionTest extends FlatSpec {

  import org.apache.log4j.{Level, Logger}
  Logger.getLogger("org.apache.spark").setLevel(Level.INFO)

  it should "correctly join stations and observations" in {

    val year: Int = 2015

    val result: Iterable[(LocalDate, Location, Double)] =
      Extraction.locateTemperatures(year, "/stations.csv",s"/$year.csv")

    result.take(10) foreach println

  }

  it should "correctly calculate averages" in {
    val year: Int = 2015

    val observations: Iterable[(LocalDate, Location, Double)] =
      Extraction.locateTemperatures(year, "/stations.csv",s"/$year.csv")

    val avgs: Iterable[(Location, Double)] = Extraction.locationYearlyAverageRecords(observations)

    avgs.take(10) foreach println
  }

}