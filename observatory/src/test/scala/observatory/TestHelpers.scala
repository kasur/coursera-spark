package observatory

import java.time.LocalDate

/**
  * Created by kasured on 6/6/17.
  */
object TestHelpers {

  def readYear(year: Int): Iterable[(Location, Double)] = {

    val observations: Iterable[(LocalDate, Location, Double)] =
      Extraction.locateTemperatures(year, "/stations.csv",s"/$year.csv")

    Extraction.locationYearlyAverageRecords(observations)
  }

}
