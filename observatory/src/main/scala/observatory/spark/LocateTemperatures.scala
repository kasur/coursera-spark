package observatory.spark

import java.time.LocalDate

import observatory.Location
import org.apache.spark.sql.{DataFrame, Dataset, Row}

import observatory.init.sparkAware

/**
  * Created by kasured on 5/10/17.
  */
final class LocateTemperatures private(val tempWithLocation: DataFrame) extends Iterable[(LocalDate, Location, Double)] {

  private[observatory] type LocalDt = (Int, Int, Int)

  private[observatory] type Export = (LocalDate, Location, Double)
  private[observatory] type Internal = (LocalDt, Location, Double)

  override def iterator: Iterator[Export] = {

    import scala.collection.JavaConverters.asScalaIteratorConverter

    val _spark = sparkAware.spark
    import _spark.implicits._

    val ds: Dataset[Internal] = tempWithLocation.map(LocateTemperatures.exportFromRow)

    ds.toLocalIterator().asScala.map( {
      case ((year, month, day), loc, temp) => (LocalDate.of(year,month,day), loc, temp)
    })
  }
}

object LocateTemperatures {

  def apply(tempWithLocation: DataFrame): LocateTemperatures = new LocateTemperatures(tempWithLocation)

  private def exportFromRow(row: Row): LocateTemperatures#Internal = {

    val (year: Int, month: Int, day: Int) = (row.getAs[Int]("year"), row.getAs[Int]("month"), row.getAs[Int]("day"))
    val (lati: Double, longi: Double) = (row.getAs[Double]("latitude"), row.getAs[Double]("longitude"))
    val temp: Double = row.getAs[Double]("temp")


    ((year, month, day), Location(lati, longi), temp)

  }
}
