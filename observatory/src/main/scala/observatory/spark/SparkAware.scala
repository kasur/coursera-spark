package observatory.spark

import org.apache.spark.sql.SparkSession

/**
  * Created by kasured on 5/6/17.
  */
trait SparkAware {
  def spark: SparkSession
}

object SparkAware {

  import org.apache.log4j.{Level, Logger}
  Logger.getLogger("org.apache.spark").setLevel(Level.INFO)

  def apply(master: String): SparkAware = new SparkAwareImpl(master)

  private[this] final class SparkAwareImpl(val master: String) extends SparkAware {
    override lazy val spark: SparkSession = SparkSession.builder()
      .master(master)
      .appName("Spark Temperature Application")
      .getOrCreate()

    sys.addShutdownHook {
      spark.stop()
    }

  }

}
