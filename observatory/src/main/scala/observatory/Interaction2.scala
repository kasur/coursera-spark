package observatory

import observatory.LayerName.{Deviations, Temperatures}

/**
  * 6th (and last) milestone: user interface polishing
  */
object Interaction2 {

  /**
    * @return The available layers of the application
    */
  def availableLayers: Seq[Layer] = {
    Seq(
      Layer(Temperatures, ColorSchema.colorPoints.toSeq, 2000 to 2015),
      Layer(Deviations, ColorSchema.colorPointsForDeviation.toSeq, 1990 to 2015)
    )
  }

  /**
    * @param selectedLayer A signal carrying the layer selected by the user
    * @return A signal containing the year bounds corresponding to the selected layer
    */
  def yearBounds(selectedLayer: Signal[Layer]): Signal[Range] = {
    Signal(selectedLayer().bounds)
  }

  /**
    * @param selectedLayer The selected layer
    * @param sliderValue The value of the year slider
    * @return The value of the selected year, so that it never goes out of the layer bounds.
    *         If the value of `sliderValue` is out of the `selectedLayer` bounds,
    *         this method should return the closest value that is included
    *         in the `selectedLayer` bounds.
    */
  def yearSelection(selectedLayer: Signal[Layer], sliderValue: Signal[Int]): Signal[Int] = {
    Signal({
      val bounds = selectedLayer().bounds
      (sliderValue(), (bounds.head, bounds.last)) match {
        case (slider: Int, (lower: Int, upper: Int)) =>
          if (slider < lower) lower
          else if (slider > upper) upper
          else slider
      }
    })
  }

  /**
    * @param selectedLayer The selected layer
    * @param selectedYear The selected year
    * @return The URL pattern to retrieve tiles
    */
  def layerUrlPattern(selectedLayer: Signal[Layer], selectedYear: Signal[Int]): Signal[String] = {
    type Id = String
    type Year = Int
    val url: (Id, Year) => String =
      (id, year) => s"target/$id/$year/{z}/{x}-{y}.png"

    Signal({
      val layer = selectedLayer()
      val year = selectedYear()
      url(layer.layerName.id, year)
    })

  }

  /**
    * @param selectedLayer The selected layer
    * @param selectedYear The selected year
    * @return The caption to show
    */
  def caption(selectedLayer: Signal[Layer], selectedYear: Signal[Int]): Signal[String] = {

    val template: (String, Int) => String = (name, year) => s"$name ($year)"

    Signal({
      val name = selectedLayer().layerName.id.capitalize
      val year = selectedYear()
      template(name, year)
    })
  }

}

sealed abstract class LayerName(val id: String)
object LayerName {
  case object Temperatures extends LayerName("temperatures")
  case object Deviations extends LayerName("deviations")
}

/**
  * @param layerName Name of the layer
  * @param colorScale Color scale used by the layer
  * @param bounds Minimum and maximum year supported by the layer
  */
case class Layer(layerName: LayerName, colorScale: Seq[(Double, Color)], bounds: Range)

