package observatory

import observatory.api.WeatherApi
import observatory.spark.SparkAware

/**
  * Created by kasured on 5/11/17.
  */
package object init {
  private[observatory] lazy val sparkAware = SparkAware("local[2]")
  private[observatory] lazy val api = WeatherApi("spark")
}
