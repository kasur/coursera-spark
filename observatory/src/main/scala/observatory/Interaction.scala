package observatory

import java.util.concurrent.atomic.AtomicInteger

import com.sksamuel.scrimage.{Image, Pixel}

import scala.collection.immutable
import scala.collection.parallel.mutable.ParArray

/**
  * 3rd milestone: interactive visualization
  */
object Interaction {

  /**
    * @param zoom Zoom level
    * @param x X coordinate
    * @param y Y coordinate
    * @return The latitude and longitude of the top-left corner of the tile, as per http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
    */
  def tileLocation(zoom: Int, x: Int, y: Int): Location = {

    import scala.math._

    val longi = x.toDouble / (1 << zoom) * 360.0 - 180.0
    val lat = atan(sinh(Pi * (1.0 - 2.0 * y.toDouble / (1 << zoom)))) * 180.0 / Pi

    Location(lon = longi, lat = lat)
  }

  /**
    * @param temperatures Known temperatures
    * @param colors Color scale
    * @param zoom Zoom level
    * @param x X coordinate
    * @param y Y coordinate
    * @return A 256×256 image showing the contents of the tile defined by `x`, `y` and `zooms`
    */
  def tile(temperatures: Iterable[(Location, Double)], colors: Iterable[(Double, Color)], zoom: Int, x: Int, y: Int): Image = {
    val locations: immutable.Seq[(Location, Int, Int)] = for {
      xx <- 0 to 255
      yy <- 0 to 255
    } yield {
      val scale = math.pow(2, zoom + 8).toInt
      val location = tileLocation(zoom + 8, x * 256 + xx, y * 256 + yy)
      (location, xx, yy)
    }

    val counter: AtomicInteger = new AtomicInteger(0)
    val ai = counter.##

    val pixelsWithLocation: ParArray[(Pixel, (Int, Int))] = locations.toParArray map {
      case triple @ (location, xC, yC) =>

        val temp = Visualization.predictTemperature(temperatures, location)
        val color @ Color(red, green, blue) = Visualization.interpolateColor(colors, temp)

        val currentCounter = counter.incrementAndGet()
        if(currentCounter % 500 == 0) {
         println(s"ai: $ai $currentCounter generated with color $color at $triple with temperature $temp")
        }

        (Pixel(red, green, blue, 127), (xC, yC))
    }

    val pixels: Array[Pixel] = Array.ofDim[Pixel](256 * 256)

    pixelsWithLocation.toArray foreach {
      case (pixel, (xC, yC)) =>
        pixels(yC * 256 + xC) = pixel
    }

    Image(256, 256, pixels)
  }

  /**
    * Generates all the tiles for zoom levels 0 to 3 (included), for all the given years.
    * @param yearlyData Sequence of (year, data), where `data` is some data associated with
    *                   `year`. The type of `data` can be anything.
    * @param generateImage Function that generates an image given a year, a zoom level, the x and
    *                      y coordinates of the tile and the data to build the image from
    */
  def generateTiles[Data](
    yearlyData: Iterable[(Int, Data)],
    generateImage: (Int, Int, Int, Int, Data) => Unit
  ): Unit = {
    for {
      (year, data) <- yearlyData
      zoom <- 0 to 3
      pow = math.pow(2, zoom).toInt
      x <- 0 until pow
      y <- 0 until pow
    } {
      generateImage(year, zoom, x, y, data)
    }
  }


  def doTile(temperatures: Iterable[(Location, Double)], colors: Iterable[(Double, Color)], zoom: Int, x: Int, y: Int): Image = {
    val locations: immutable.Seq[(Location, Int, Int)] = for {
      xx <- 0 to 255
      yy <- 0 to 255
    } yield {
      val scale = math.pow(2, zoom + 8).toInt
      val location = tileLocation(zoom + 8, x * 256 + xx, y * 256 + yy)
      (location, xx, yy)
    }

    //val counter: AtomicInteger = new AtomicInteger(0)

    val pixelsWithLocation: ParArray[(Pixel, (Int, Int))] = locations.toParArray map {
      case triple @ (location, xC, yC) =>

        val temp = Visualization.predictTemperature(temperatures, location)
        val color @ Color(red, green, blue) = Visualization.interpolateColor(colors, temp)

        /*val currentCounter = counter.incrementAndGet()
        if(currentCounter % 500 == 0) {
         println(((s"$currentCounter generated with color $color at $triple with temperature $temp")
        }*/

        (Pixel(red, green, blue, 127), (xC, yC))
    }

    val pixels: Array[Pixel] = Array.ofDim[Pixel](256 * 256)

    pixelsWithLocation.toArray foreach {
      case (pixel, (xC, yC)) =>
        pixels(yC * 256 + xC) = pixel
    }

    Image(256, 256, pixels)
  }

}
