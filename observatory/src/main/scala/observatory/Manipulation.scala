package observatory
import java.util.concurrent.atomic.AtomicInteger

import scala.collection.immutable
import scala.collection.parallel.immutable.ParSeq

/**
  * 4th milestone: value-added information
  */
object Manipulation {

  /**
    * @param temperatures Known temperatures
    * @return A function that, given a latitude in [-89, 90] and a longitude in [-180, 179],
    *         returns the predicted temperature at this location
    */
  def makeGrid(temperatures: Iterable[(Location, Double)]): (Int, Int) => Double = {

    val locations: immutable.Seq[(Int, Int)] = for {
      lat <- -89 to 90
      lon <- -180 to 179
    } yield (lat, lon)

    val counter: AtomicInteger = new AtomicInteger(0)
    val hash = counter.##

    val approxGrid: ParSeq[((Int, Int), Double)] =
      locations.par.map(location => {
        val result = (location,Visualization.predictTemperature(temperatures, Location(location._1, location._2)))

        val currentCounter = counter.incrementAndGet()
        if(currentCounter % 500 == 0) {
         println(s"ai $hash: $currentCounter predicted result in makeGrid for $result")
        }

        result
      })

    val _reference: Map[(Int, Int), Double] = approxGrid.seq.toMap

    (latitude: Int, longitude: Int) => _reference((latitude, longitude))
  }


  /**
    * @param temperatures Sequence of known temperatures over the years (each element of the collection
    *                      is a collection of pairs of location and temperature)
    * @return A function that, given a latitude and a longitude, returns the average temperature at this location
    */
  def average(temperatures: Iterable[Iterable[(Location, Double)]]): (Int, Int) => Double = {

    type Grid = (Int, Int) => Double

    println(s"[erusak] Start yearlyGrids")
    val yearlyGrids: Iterable[Grid] = temperatures.par.map(makeGrid).seq
    println(s"[erusak] Finish yearlyGrids")

    val locations: immutable.Seq[(Int,Int)] = for {
      lat <- -89 to 90
      lon <- -180 to 179
    } yield (lat, lon)

    println(s"[erusak] Start avgGrid")
    val avgGrid: Seq[((Int,Int), Double)] =  locations.map(location => {

        val zero: (Double, Int) = (0.0d, 0)

        val (sum: Double, count: Int) = (zero /: yearlyGrids){ (aggregate, grid) =>
          val prediction: Double = grid(location._1, location._2)
          val (cSum, cCount) = aggregate
          (cSum + prediction, cCount + 1)
        }

      (location, sum / count)

      }).seq

    val _reference = avgGrid.toMap
    println(s"[erusak] Finish avgGrid")

    (latitude: Int, longitude: Int) => _reference((latitude, longitude))


  }

  /**
    * @param temperatures Known temperatures
    * @param normals A grid containing the “normal” temperatures
    * @return A grid containing the deviations compared to the normal temperatures
    */
  def deviation(temperatures: Iterable[(Location, Double)], normals: (Int, Int) => Double): (Int, Int) => Double = {

    val grid: (Int, Int) => Double = makeGrid(temperatures)

    val locations: immutable.Seq[(Int,Int)] = for {
      lat <- -89 to 90
      lon <- -180 to 179
    } yield (lat, lon)

    println(s"[erusak] Start deviation")
    val parSeq: ParSeq[((Int, Int), Double)] = locations.par.map { case location@(lat, lon) =>
      val normal = normals(lat, lon)
      val known = grid(lat, lon)

      val deviation = known - normal

      location -> deviation
    }

    val devGrid: Map[(Int, Int), Double] = parSeq.seq.toMap

    println(s"[erusak] Finish deviation")

    (latitude: Int, longitude: Int) => devGrid((latitude, longitude))

  }


}

