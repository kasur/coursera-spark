package observatory

import java.util.concurrent.atomic.AtomicInteger

import com.sksamuel.scrimage.{Image, Pixel}

import scala.collection.immutable
import scala.collection.parallel.mutable.ParArray

/**
  * 5th milestone: value-added information visualization
  */
object Visualization2 {

  type Point = (Int, Int)
  type Square = (Point, Point, Point, Point)

  /**
    * @param x X coordinate between 0 and 1
    * @param y Y coordinate between 0 and 1
    * @param d00 Top-left value
    * @param d01 Bottom-left value
    * @param d10 Top-right value
    * @param d11 Bottom-right value
    * @return A guess of the value at (x, y) based on the four known values, using bilinear interpolation
    *         See https://en.wikipedia.org/wiki/Bilinear_interpolation#Unit_Square
    */
  def bilinearInterpolation(
    x: Double,
    y: Double,
    d00: Double,
    d01: Double,
    d10: Double,
    d11: Double
  ): Double = {
    d00 * (1 - x) * (1 - y) + d10 * x * (1 - y) + d01 * (1 - x) * y + d11 * x * y
  }

  /*def findSquareForLocation(location: Location): Square = {
    import scala.math.{signum, abs, floor}

    val (lat, lon) = (location.lat, location.lon)

    val topLeft @ (topLeftLat: Int, topLeftLon: Int) = (
      (signum(lat) * floor(abs(lat))).toInt,
      (signum(lon) * floor(abs(lon))).toInt
    )
    val topRight = (topLeftLat, topLeftLon + 1)
    val bottomLeft = (topLeftLat - 1, topLeftLon)
    val bottomRight = (topLeftLat - 1, topLeftLon + 1)

    (topLeft.cut, topRight.cut, bottomLeft.cut, bottomRight.cut)

  }*/

  def findSquareForLocation2(location: Location): Square = {
    import scala.math.floor

    val (lat, lon) = (location.lat, location.lon)

    val topRight @ (topRightLat: Int, topRightLon: Int) = (
      floor(lat).toInt,
      floor(lon).toInt
    )
    val topLeft = (topRightLat + 1, topRightLon)
    val bottomLeft = (topRightLat + 1, topRightLon + 1)
    val bottomRight = (topRightLat, topRightLon + 1)

    (topLeft.cut, topRight.cut, bottomLeft.cut, bottomRight.cut)

  }

  private implicit class EdgeNormalizer(val point: Point) extends AnyVal {
    def cut: Point = point match {
      case (latE, lonE) =>
        val lat = if(latE > 90) 90 else if (latE < -89) -89 else latE
        val lon = if(lonE > 179) 179 else if (lonE < -180) -180 else lonE
        (lat, lon)
    }
  }

  /**
    * @param grid Grid to visualize
    * @param colors Color scale to use
    * @param zoom Zoom level of the tile to visualize
    * @param x X value of the tile to visualize
    * @param y Y value of the tile to visualize
    * @return The image of the tile at (x, y, zoom) showing the grid using the given color scale
    */
  def visualizeGrid(
    grid: (Int, Int) => Double,
    colors: Iterable[(Double, Color)],
    zoom: Int,
    x: Int,
    y: Int
  ): Image = {

    val tupledGrid: ((Int, Int)) => Double = grid.tupled

    val locations: immutable.Seq[(Location, Int, Int)] = for {
      xx <- 0 to 255
      yy <- 0 to 255
    } yield {
      val location = Interaction.tileLocation(zoom + 8, x * 256 + xx, y * 256 + yy)
      (location, xx, yy)
    }

    val counter: AtomicInteger = new AtomicInteger(0)

    val pixelsWithLocation: ParArray[(Pixel, (Int, Int))] = locations.toParArray map {
      case triple @ (location, xC, yC) =>

        val square: Square = findSquareForLocation2(location)

        //val (topLeft @ (topLeftLat, topLeftLon), topRight, bottomLeft, bottomRight) = square
        val (topLeft, topRight @ (topRightLat, topRightLon), bottomLeft, bottomRight) = square

        val (d00, d10, d01, d11) = (
          tupledGrid(topRight),
          tupledGrid(topLeft),
          tupledGrid(bottomRight),
          tupledGrid(bottomLeft)
        )

        val (lat, lon) = (location.lat - topRightLat, location.lon - topRightLon)

        val temp = bilinearInterpolation(lat, lon, d00, d01, d10, d11)
        val color @ Color(red, green, blue) = Visualization.interpolateColor(colors, temp)

        val currentCounter = counter.incrementAndGet()
        if(currentCounter % 500 == 0) {
         println(s"$currentCounter generated with color $color at $triple with temperature $temp")
        }

        (Pixel(red, green, blue, 127), (xC, yC))
    }

    val pixels: Array[Pixel] = Array.ofDim[Pixel](256 * 256)

    pixelsWithLocation.toArray foreach {
      case (pixel, (xC, yC)) =>
        pixels(yC * 256 + xC) = pixel
    }

    Image(256, 256, pixels)

  }

}
