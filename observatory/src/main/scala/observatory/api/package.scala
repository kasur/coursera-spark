package observatory

/**
  * Created by kasured on 5/11/17.
  */
package object api {

  implicit final class StrategyWrapper(val strategy: String) extends AnyVal {
    def toStrategy: Strategy = strategy.toLowerCase match {
      case "spark" => Spark
    }
  }
}
