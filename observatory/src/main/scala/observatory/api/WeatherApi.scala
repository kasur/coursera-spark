package observatory.api

import java.time.LocalDate

import observatory.Location

/**
  * Created by kasured on 5/11/17.
  */
trait WeatherApi {
  def locateTemperatures(year: Int, stationsFile: String, temperaturesFile: String): Iterable[(LocalDate, Location, Double)]
  def locationYearlyAverageRecords(records: Iterable[(LocalDate, Location, Double)]): Iterable[(Location, Double)]
}

object WeatherApi {
  def apply(strategy: String): WeatherApi = strategy.toStrategy match {
    case Spark => new WeatherApiSpark
  }
}
