package observatory.api
import java.time.LocalDate

import observatory.Helpers.fsPath
import observatory._
import observatory.api.WeatherApiSpark.{observationsForYearInCelsius, readObservations, readStations}
import observatory.init.sparkAware
import observatory.spark.LocateTemperatures
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Row}

/**
  * Created by kasured on 5/11/17.
  */
private[api] final class WeatherApiSpark extends WeatherApi {

  override def locateTemperatures(year: Int, stationsFile: String, temperaturesFile: String): Iterable[LocateTemperatures#Export] = {
    
    val stations: DataFrame = readStations(fsPath(stationsFile))
    val observations: DataFrame = readObservations(fsPath(temperaturesFile))

    val result: DataFrame = observationsForYearInCelsius(year, stations, observations)

    LocateTemperatures(result)
  }

  override def locationYearlyAverageRecords(records: Iterable[LocateTemperatures#Export]): Iterable[(Location, Double)] = {
    val grouped: Map[Location, Iterable[(LocalDate, Location, Double)]] =
      records.filter(WeatherApiSpark.excludeAnomalies).groupBy({case (_, location, _ ) => location})

    val avg: Map[Location, Double] = grouped map {
      case (location, observations) => location -> observations.map(_._3).sum / observations.size
    }

    avg

  }

}

object WeatherApiSpark {

  private def excludeAnomalies(observation: LocateTemperatures#Export): Boolean = observation match {
    case (_, _, temp) => temp <= 5000.0d // scalastyle:ignore
  }

  private def readStations(path: String): DataFrame = {
    val lines: RDD[String] = sparkAware.spark.sparkContext.textFile(path)
    val rows: RDD[Row] = lines.map(line => line.split(",").toList).map(stationRow)

    sparkAware.spark.createDataFrame(rows, stationSchema)

  }

  private def readObservations(path: String): DataFrame = {
    val lines: RDD[String] = sparkAware.spark.sparkContext.textFile(path)
    val rows: RDD[Row] = lines.map(line => line.split(",").toList).map(observationRow)

    sparkAware.spark.createDataFrame(rows, observationSchema)

  }

  private def stationSchema: StructType = StructType(
    StructField("stn", StringType, nullable = true)
      :: StructField("wban", StringType, nullable = true)
      :: StructField("latitude", DoubleType, nullable = true)
      :: StructField("longitude", DoubleType, nullable = true)
      :: Nil
  )

  private def observationSchema: StructType = StructType(
    StructField("stn", StringType, nullable = true)
      :: StructField("wban", StringType, nullable = true)
      :: StructField("month", IntegerType, nullable = false)
      :: StructField("day", IntegerType, nullable = false)
      :: StructField("temp", DoubleType, nullable = false)
      :: Nil
  )

  private def stationRow(list: List[String]) : Row = {
    val listO: (Int) => Option[String] = list.lift


    val stn = listO(0).map(_stn => if(_stn.trim().isEmpty) null.asInstanceOf[String] else _stn).getOrElse(null.asInstanceOf[String])
    val wban = listO(1).map(_wban => if(_wban.trim().isEmpty) null.asInstanceOf[String] else _wban).getOrElse(null.asInstanceOf[String])

    val vals: Seq[Any] =
      stn :: wban :: listO(2).map(_.toDouble).getOrElse(0.0d) :: listO(3).map(_.toDouble).getOrElse(0.0d) :: Nil

    Row.fromSeq(vals)

  }

  // scalastyle:off
  private def observationRow(list: List[String]): Row = {
    val lifted: (Int) => Option[String] = list.lift

    val stn = lifted(0).map(_stn => if(_stn.trim().isEmpty) null.asInstanceOf[String] else _stn).getOrElse(null.asInstanceOf[String])
    val wban = lifted(1).map(_wban => if(_wban.trim().isEmpty) null.asInstanceOf[String] else _wban).getOrElse(null.asInstanceOf[String])

    val vals: Seq[Any] =
      stn :: wban :: lifted(2).map(_.toInt).get :: lifted(3).map(_.toInt).get :: lifted(4).map(_.toDouble).getOrElse(0.0d) :: Nil

    Row.fromSeq(vals)
  }

  private def observationWithLocation(stations: DataFrame, observations: DataFrame): DataFrame = {
    val _sqlContext = sparkAware.spark.sqlContext
    import _sqlContext.implicits._

    val stationsWithLocations: DataFrame = stations.filter($"latitude" =!= 0.0d && $"longitude" =!= 0.0d)

    stationsWithLocations.as("st").join(observations.as("ob"), $"st.stn" <=> $"ob.stn"
      && $"st.wban" <=> $"ob.wban", "inner").select($"st.latitude", $"st.longitude", $"ob.*")
  }

  private def observationsForYearInCelsius(year: Int, stations: DataFrame, observations: DataFrame): DataFrame = {
    val _sqlContext = sparkAware.spark.sqlContext

    import _sqlContext.implicits._

    val observationWithLocations = observationWithLocation(stations, observations)

    import org.apache.spark.sql.functions.{lit, udf}

    val toCels: UserDefinedFunction = udf[Double,Double](Helpers.toCelsius)

    observationWithLocations.withColumn("year", lit(year)).withColumn("temp", toCels($"temp"))

  }
}
