package observatory

case class Location(lat: Double, lon: Double)

case class LocationInt(lat: Int, lon: Int)

case class Color(red: Int, green: Int, blue: Int)

object ColorSchema {
  val rgbFor: Map[Symbol, Color] = Map(
    'white -> Color(255,255,255),
    'red -> Color(255, 0, 0),
    'orange -> Color(255, 153, 0),
    'yellow -> Color(255, 255, 0),
    'cyan -> Color(0, 255, 255),
    'blue -> Color(0, 0, 255),
    'magenta -> Color(255, 0, 255),
    'darkblue -> Color(33, 0, 107),
    'black -> Color(0, 0, 0)
  )

  val tempFor: Map[Symbol, Double] = Map(
    'white -> 60.0d,
    'red -> 32.0d,
    'orange -> 20.0d,
    'yellow -> 12.0d,
    'cyan -> 0.0d,
    'blue -> -15.0d,
    'magenta -> -27.0d,
    'darkblue -> -50.0d,
    'black -> -60.0d
  )

  final val tempForDeviation: Symbol => Double = Map(
    'black -> 7.0d,
    'red -> 4.0d,
    'yellow -> 2.0d,
    'white -> 0.0d,
    'cyan -> -2.0d,
    'blue -> -7.0d
  )

  val PointFor: Symbol => (Double, Color) = name => (tempFor(name), rgbFor(name))

  val PointForDeviation: Symbol => (Double, Color) = name => (tempForDeviation(name), rgbFor(name))

  val colorPoints: Iterable[(Double, Color)] = Iterable(
    PointFor('white),
    PointFor('red),
    PointFor('yellow),
    PointFor('cyan),
    PointFor('blue),
    PointFor('magenta),
    PointFor('darkblue),
    PointFor('black)
  )

  val colorPointsForDeviation: Iterable[(Double, Color)] = Iterable(
    PointForDeviation('black),
    PointForDeviation('red),
    PointForDeviation('yellow),
    PointForDeviation('white),
    PointForDeviation('cyan),
    PointForDeviation('blue)
  )
}

