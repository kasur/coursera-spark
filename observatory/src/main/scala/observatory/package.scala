import java.nio.file.Paths

import com.sksamuel.scrimage.Pixel

/**
  * Created by kasured on 5/6/17.
  */
package object observatory {

  object Helpers {
    def toCelsius(fahr: Double): Double = (fahr - 32) * 5 * 1000 / 9 / 1000
    def fsPath(resource: String): String =
      Paths.get(getClass.getResource(resource).toURI).toString
  }

  private[observatory] implicit class ColorPointHolder(val point: (Double, Color)) extends AnyVal {
    def temperature: Double = point._1
    def color: Color = point._2
  }

  private[observatory] implicit class PixelWrapper(val pair: (LocationInt, Pixel)) extends AnyVal {
    def toArrayCoordinate: ((Int, Int), Pixel) = {
      val (location, pixel) = pair
      val (x: Int, y: Int) = (180 + location.lon, 90 - location.lat)
      ((x, y), pixel)
    }
  }

}
