package observatory

import com.sksamuel.scrimage.{Image, Pixel}

import scala.annotation.tailrec
import scala.collection.immutable
import scala.collection.parallel.immutable.ParSeq
import scala.math.{abs, acos, cos, sin}

/**
  * 2nd milestone: basic visualization
  */
object Visualization {

  /**
    * @param temperatures Known temperatures: pairs containing a location and the temperature at this location
    * @param location Location where to predict the temperature
    * @return The predicted temperature at `location`
    */
  def predictTemperature(temperatures: Iterable[(Location, Double)], location: Location): Double = {

    val p: Int = 3

    val rkm: Int = 6371

    def rad(deg: Double): Double = math.toRadians(deg)

    def dx(y: Location): Double = rkm * acos {
        val arg: Double =
          sin(rad(location.lat)) * sin(rad(y.lat)) + cos(rad(location.lat)) * cos(rad(y.lat)) * cos(abs(rad(location.lon - y.lon)))
        if(-1.0d <= arg && arg <= 1.0d) arg else if(arg > 1.0d) 1.0d else -1.0d
    }

    def wx(y: Location): Double = math.pow(dx(y), -p)

    def func(numDenom: (Double,Double), point: (Location, Double)): (Double, Double) = {

      val (num: Double, denom: Double) = numDenom
      val (loc, temperature) = point

      val weight = wx(loc)

      val numNew = num + weight * temperature

      val denomNew = denom + weight

      (numNew,denomNew)

    }

    def ux: Double = temperatures find(point => dx(point._1) <= 1.0d) match {

      case Some((_, someTemperature)) =>
        someTemperature
      case None =>
        val (num: Double, denum: Double) = ((0.0d, 0.0d) /: temperatures.par) (func)
        num / denum
    }

    ux

  }

  /**
    * @param points Pairs containing a value and its associated color
    * @param value The value to interpolate
    * @return The color that corresponds to `value`, according to the color scale defined by `points`
    */
  def interpolateColor(points: Iterable[(Double, Color)], value: Double): Color = {

    type Point = (Double, Color)

    val pointsSorted: Seq[Point] = points.toSeq.sortBy(_._1)

    val (min, max) = (pointsSorted.head, pointsSorted.last)

    def findPair: (Point, Point) = _find(pointsSorted)

    @tailrec
    def _find(pts: Seq[Point]): (Point, Point) = pts match {
      case Nil =>
        throw new IllegalArgumentException(s"No match for value ${value} in colors ${pointsSorted}")
      case p1 :: tail =>
        if(p1.temperature <= value && value < tail.head.temperature) {
          //println((s"found points $p1 and ${tail.head}")
          (p1, tail.head)
        } else {
          _find(tail)
        }
    }

    def linear(t1: Double, t2: Double)(c1: Int, c2: Int): Int = (c1 + (c2 - c1) * (value - t1) / (t2 - t1)).round.toInt

    def interpolate(p1: Point, p2: Point): Point = {
      val t1: Double = p1._1
      val t2: Double = p2._1

      val lin: (Int, Int) => Int = linear(t1,t2)

      val Color(red1, green1, blue1) = p1._2
      val Color(red2, green2, blue2) = p2._2

      val colI: Color = Color(lin(red1, red2), lin(green1, green2), lin(blue1,blue2))

      (value, colI)

    }

    if(value >= max.temperature) {
      max.color
    } else if (value <= min.temperature) {
      min.color
    } else {
      val result = findPair match {
        case (p1, p2) => interpolate(p1, p2)
      }

      result._2
    }


  }

  /**
    * @param temperatures Known temperatures
    * @param colors Color scale
    * @return A 360×180 image where each pixel shows the predicted temperature at its location
    */
  def visualize(temperatures: Iterable[(Location, Double)], colors: Iterable[(Double, Color)]): Image = {

    //println((s"Start location map initialization")

    val locationMap: immutable.Seq[LocationInt] = for (lat <- -89 to 90; lon <- -180 to 179 ) yield LocationInt(lat, lon)

    //println((s"Finish location map initialization")

    //println((s"Start pixSeqPar initialization")

    //val atomic: AtomicInteger = new AtomicInteger(0)

    val pixSeqPar: ParSeq[(LocationInt, Pixel)] = locationMap.par map { location =>

      val temperature: Double = predictTemperature(temperatures, Location(location.lat, location.lon))
      val Color(red, green, blue) = interpolateColor(colors, temperature)

      /*val total = atomic.incrementAndGet()
      val isPrint = total % 500 == 0
      if(isPrint) {
        println((s"Total pixels processed $total")
      }*/

      (location, Pixel(red, green, blue, 255))

    }

    //println((s"Finish pixSeqPar initialization")

    val pixels: Array[Pixel] = Array.ofDim[Pixel](360 * 180)

    //println((s"Start Transform pixSeqPar initialization")

    pixSeqPar.seq foreach { pair =>
      val ((x: Int, y: Int), pixel) = pair.toArrayCoordinate
      pixels(y * 360 + x) = pixel
      //println((s"pixel for location ${pair._1} processed")
    }

    //println((s"Finish Transform pixSeqPar initialization")

    //println((s"Start Image initialization")
    val image = Image(360, 180, pixels)
    //println((s"Finish Image initialization")

    image
  }

}

