package stackoverflow

import org.apache.spark.rdd.RDD
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfterAll, FunSuite}
import stackoverflow.StackOverflow._

@RunWith(classOf[JUnitRunner])
class StackOverflowSuite extends FunSuite with BeforeAndAfterAll {


  lazy val testObject = new StackOverflow {
    override val langs =
      List(
        "JavaScript", "Java", "PHP", "Python", "C#", "C++", "Ruby", "CSS",
        "Objective-C", "Perl", "Scala", "Haskell", "MATLAB", "Clojure", "Groovy")
    override def langSpread = 50000
    override def kmeansKernels = 45
    override def kmeansEta: Double = 20.0D
    override def kmeansMaxIterations = 120
  }

  test("testObject can be instantiated") {
    val instantiatable = try {
      testObject
      true
    } catch {
      case _: Throwable => false
    }
    assert(instantiatable, "Can't instantiate a StackOverflow object")
  }


  test("scoredPostings results should contain expected values") {

    val lines   = sc.textFile("src/main/resources/stackoverflow/stackoverflow.csv")

    val raw     = rawPostings(lines)
    //println(s"Number of raw ${raw.count()}")

    val grouped = groupedPostings(raw)
    //println(s"Number of grouped ${grouped.count()}")

    val scored  = scoredPostings(grouped)

    val vectors = vectorPostings(scored)

    //assert(vectors.count() == 2121822, "Incorrect number of vectors: " + vectors.count())

    val testData = List[(Posting, Int)](
      (Posting(1,6,None,None,140,Some("CSS")), 67),
      (Posting(1,42,None,None,155,Some("PHP")),89),
      (Posting(1,72,None,None,16,Some("Ruby")),3),
      (Posting(1,126,None,None,33,Some("Java")),30),
      (Posting(1,174,None,None,38,Some("C#")),20)
    )

    val result: RDD[(Posting, Int)] = sc.parallelize(testData)

    assert(result.intersection(scored).collect().toSeq.sortBy(_._2) === testData.sortBy(_._2))

  }

}
